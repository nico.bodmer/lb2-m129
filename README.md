 # 1. Auftrag 
## Netzwerkplan
![](./image/1.png)

## Vorgaben
- Vom ISP erhalten sie den IP-Range: 160.160.250.0 /24 
- Erstellen Sie zwei gleichgrosse Subnetze - für jede Abteilung eines.
- Die erste IP-Adresse jedes Subnets ist für den Router reserviert.
- Die Endgeräte erhalten die näthöheren IP-Adressen (z.B. PC-01 160.160.250.2)
- Namenskonvention (gemäss Netzwerkplan)

## Beschreibung
### Excel-Sheet Vorgehen
Um 2 gleich grosse Subnetze zu erstellen wird das Kuchennetzstück durch 2 geteilt

/25 -> 2 gleich grosse Subnetze 

Zuerst habe ich die neue Netzmaske in der Tabelle dargestellt. Daraufhin habe ich die Netzwerk-ID in der Tabelle dargestellt und den Schwarzen Strich für das /25er Sunbetz gezogen. Danach konnte ich ganz einfach die First IP, Last IP und den Broadcast ergänzen.  

Da das Netz in 2 gleich grosse Stücke geteilt wird kann man das letzte Oktett durch 2 teilen 256/2 = 128. Wir haben also immer 128 mögliche IPs in einem Subnetz (-2, denn durch die Netz-ID und den Broadcast können wir nur 126 IPs vergeben)

Die Netze sind also wie folgt aufgeteilt:
0-127, 128-255. Der erste und der letzte Wert sind dabei immer Netz-ID oder Broadcast. Mit diesen Informationen konnte ich die 2. Netzwerktabellen ausfüllen.
![](./images/1.jpg)

### Filius-File Vorgehen
1. Schritt: bei allen Geräten die Subnetzmaske 255.255.255.128 ergänzen
2. Schritt: beim Router die richtigen Ports mit den passenden IPs versehen -> 160.160.250.1 und 160.160.250.129
3. Schritt: bei allen Clients die richtigen IP-Adressen der Ports beim Gateway eintragen
4. Schritt: bei allen Geräten die IPs hinzufügen
z.B. Pc-2 bekommt die nächst verfügbare IP-Adresse nach 160.160.250.1. Das wäre 160.160.250.2. Pc-3 bekommt danach die nächst verfügbare IP-Adresse. Das wäre 160.160.250.3 ...

Danach habe ich die Konsole von einem Client geöffnet und dem anderen Client im gleichen Subnet gepingt. Als das erfolgreich war habe ich noch probiert einem Client aus dem anderen Subnet zu pingen, um sicher zugehen, dass die Einstellungen der Clients und des Gateway/Router richtig sind.
![](./images/3.jpg)